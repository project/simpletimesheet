
--------------------------------------------------------------------------------
Simple TimeSheet
--------------------------------------------------------------------------------

Maintainer:  xlyz, xlyz@tiscali.it

Provides very basic functionalities to handle timesheets in Drupal 7.

This module has been developed as none of the many timesheet / timerecord 
6.x modules has been ported to Drupal 7.x.

Some of the dependencies have issues. Please use latest releases and check
simpletimesheet project homepage to see what issues are still open and what
are existing workaround.

Poject homepage: http://drupal.org/project/simpletimesheet

Issues: http://drupal.org/project/issues/simpletimesheet


Installation
------------

 * Copy the whole simpletimesheet directory to your modules directory
   (e.g. DRUPAL_ROOT/sites/all/modules) and activate it in the modules page

 * Assign "Timesheet access" permission to roles that can use this functionality
 
 * ...

 * Profit!  :)
   

Documentation
-------------

This module use fields and views as much as possible to allow easy
customization.

At least one content type "activity" has to be created. Users have to belong to
a role with "TimeSheet access" permission to be selectable as member.

Each user can create only time records for activities they are member of.

"Timesheet access" permission is required too to view timesheets reports.
